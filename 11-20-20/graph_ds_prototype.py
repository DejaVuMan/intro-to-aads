from enum import Enum
from typing import Any
from typing import Optional
from typing import Dict, List

puzzle = __import__("OneWayList_WW")

class Queue:
    def __init__(self):
        self.queue = puzzle.LinkedList()

    def __len__(self):
        return len(self.queue)

    def __repr__(self):
        holder = self.queue.head.next
        # the .next part here is necessary to not display the
        # "dummy" starter element of none.
        disp = str(holder.data)
        holder = holder.next
        while holder is not None:
            disp += ' -> ' + str(holder.data)
            holder = holder.next
        return disp

    def peekaboo(self):
        return self.queue.head.value

    def enq(self, data):
        self.queue.append(data)

    def deq(self):
        return self.queue.pop()


class EdgeType(Enum):
    directed = 1
    undirected = 2

class Vertex:
    data: Any
    index: int

    def __init__(self, data, idx):
        self.data = data
        self.index = idx

    def __repr__(self):
        return f'{self.data} of index {self.index}'

class Edge:
    source: Vertex
    destination: Vertex
    weight: Optional[float]

    def __init__(self, source, dest, imp):
        self.source = source
        self.destination = dest
        self.weight = imp
        # Weight is not needed

    def __repr__(self):
        return f'Target: {self.destination}'

class Graph:
    adjacency: Dict[Vertex, List[Edge]]

    def __init__(self):
        self.adjacency = dict()

    def create_vertex(self, data: Any):
        self.adjacency[Vertex(data, len(self.adjacency))] = list()

    def add_diredge(self, source: Vertex, destination: Vertex,
                    weight: Optional[float] is None):
        driver.adjacency[source].append(Edge(source, destination, weight))
        # by setting driver equal to Graph() outside of this code, we can basically run this code properly for the
        # instance of that name
        # Will only add one directional support to next node

    def add_anyedge(self, source: Vertex, destination: Vertex,
                    weight: Optional[float] is None):
        driver.adjacency[source].append(Edge(source, destination, weight))
        driver.adjacency[destination].append(Edge(source, destination, weight))
        # Will add bi directional support to next node

    def add(self, edge: EdgeType, source: Vertex, destination: Vertex,
            weight: Optional[float] = None):
        if edge.name is "directed":
            self.add_diredge(source, destination, weight)
        else:
            self.add_anyedge(source, destination, weight)

    def depth_first_trav(self, goto):
        visited = []
        klist = []
        for ct in self.adjacency.keys():
            klist.append(ct)
        new = klist[0]
        self._dfs(new, visited, goto)

    def _dfs(self, v: Vertex, viewed: List[Vertex], visit):
        visit(v)
        viewed.append(v)
        for next_node in self.adjacency[v]:
            if next_node.destination in viewed:
                return True
            else:
                self._dfs(next_node.destination, viewed, visit)

    def breadth_first_trav(self, goto):
        visited = []
        klist = []
        for ct in self.adjacency.keys():
            klist.append(ct)
        line = Queue()
        line.enq(klist[0])
        while len(line) != 0:
            new_n = line.deq()
            visited.append(new_n)
            goto(new_n)
            for next_node in self.adjacency[new_n]:
                if next_node.destination in visited:
                    return True
                else:
                    line.enq(next_node.destination)
            # TODO: Fix Breadth First Traversal


driver = Graph()
driver.create_vertex(10)
driver.create_vertex(20)
driver.create_vertex(69)
driver.create_vertex("final")

tester = driver.adjacency.keys()
keylist = []
for x in tester:
    keylist.append(x)
driver.add(EdgeType(1), keylist[0], keylist[1], 1)  # 10 targets 20, weight of 1
driver.add(EdgeType(1), keylist[0], keylist[2], 3)  # 10 targets 69, weight of 3
driver.add(EdgeType(1), keylist[2], keylist[3], 10)  # 69 targets "final", weight of 10

print(driver.adjacency)
driver.depth_first_trav(print)
print()
driver.breadth_first_trav(print)
print()

print(driver)

"""queue = Queue()

assert len(queue) == 0

queue.enq('test')
queue.enq('test2')

print(str(queue))
print(len(queue))"""
