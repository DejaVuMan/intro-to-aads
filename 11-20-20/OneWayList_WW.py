#  The name of this file is just a meme, this is still a Linked list

"""  Final version should contain:

Node Class |+
LinkedList Class |+
push method to make new node at start of list |+
append method to make new node at end of list |+
node method which returns a node in the given position |+
insert method which inserts a node after a certain position |+
pop method which removes first element and displays it |+
remove_last method which removes the last element from the list and displays it |+ !!!
remove method which removes a certain node |+
print method which shows elements in order 0 -> 1... |+
len method which returns the length of array |+
"""


class Node:
    def __init__(self, data=None):
        self.data = data  # Past data point
        self.next = None  # Pointer to next node

    def __repr__(self):  # Def __repr__, more like Death Reaper haha
        if self.next is None:
            return f'{self.data}'
        if self.data is None:  # Necessary to not display the "Dummy" element
            return f'{self.next}'
        return f'{self.data} -> {self.next}'


class LinkedList:
    def __init__(self):
        self.head = Node()
        # Have Node always available inside of LinkedList. Won't contain actual data. Not indexable.
        # Placeholder to point to first element in a list.

    def append(self, data):  # Used to create first element.
        new_node = Node(data)
        current = self.head  # The thing we're currently looking at will be the head, left-most element.
        while current.next is not None:
            current = current.next
        current.next = new_node
        return

    def push(self, data):
        current_node = self.head
        before_node = self.head
        current_node = current_node.next
        new_node = Node(data)
        before_node.next = new_node
        new_node.next = current_node
        return

    def length(self):
        current = self.head
        total = 0
        while current.next is not None:
            total += 1
            current = current.next
        return total

    def __len__(self):
        current = self.head
        total = 0
        while current.next is not None:
            total += 1
            current = current.next
        return total

    def node(self, index):
        if index >= self.length() or index < 0:  # We should communicate to the user if they enter a bad index.
            print("ERROR: 'Get' Index out of range!")
            return None
        current_index = 0
        current_node = self.head
        while True:
            current_node = current_node.next
            if current_index == index:
                return current_node.data
            current_index += 1

    def pop(self):
        current_node = self.head
        last_node = current_node
        # Book keeping purposes: When we remove a node, we want to make sure it points to the correct next node.
        current_node = current_node.next  # Increment to next node.
        last_node.next = current_node.next  # Effectively, this is like erasing the current node.
        return current_node.data

    def insert(self, index, data):
        if index >= self.length() or index < 0:
            return self.append(data)
        current_node = self.head
        before_node = self.head
        current_index = 0
        while True:
            current_node = current_node.next
            if current_index == index:
                new_node = Node(data)
                before_node.next = new_node
                new_node.next = current_node
                return
            before_node = current_node
            current_index += 1

    def remove(self, index):
        if index >= self.length():
            print("ERROR: 'Get' Index out of range...")
            return None
        current_index = 0
        current_node = self.head
        while True:
            last_node = current_node
            # Book keeping purposes: When we remove a node, we want to make sure it points to the correct next node.
            current_node = current_node.next  # Increment to next node.
            if current_index == index:
                last_node.next = current_node.next  # Effectively, this is like erasing the current node.
                return
            current_index = current_index + 1

    def remove_last(self):
        current_index = 0
        current_node = self.head
        while True:
            last_node = current_node
            # Book keeping purposes: When we remove a node, we want to make sure it points to the correct next node.
            current_node = current_node.next  # Increment to next node.
            if current_index == (self.length() - 1):
                # print("Node to be removed from end of linked list: " + str(elements))
                last_node.next = current_node.next  # Effectively, this is like erasing the current node.
                return current_node.data
            current_index = current_index + 1

    def __repr__(self):  # "Magical Function
        return f'{self.head}'


"""
So, from my understanding, there are two methods to implement a LinkedList.

The first one is to make the head none when there are zero elements in the list. This is often easier to think
about, as you know that the head always refers to the first element and so on. The downside is that you have to 
check if the head is not none everytime it's accessed.

The second way, which is the way that I went, uses a "dummy" link/element that acts as the head of the chain.
This element will never have a value, as it just acts as a pointer to the first element. The benefit of my approach
is that I can just check if the next node is none for all functions because the head is guaranteed to exist. The
downside of this, of course, is that I have to be very careful when "exposing" the internals of the list and 
not showing the first "dummy" element exists.
"""

