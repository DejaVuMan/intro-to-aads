import arcade
class GameWindow(arcade.Window):
    MUSIC_VOLUME = 0.5

    def __init__(self, width, height, title):
        super().__init__(width, height, title)
        self.set_location(400, 200)
        self.music_list = ["music/start.mp3"]
        self.current_song = 0
        self.music = None

        arcade.set_background_color(arcade.color.BLACK)
        self.player_x = 100
        self.player_y = 200
        self.player_speed = 250

        self.spritemain = arcade.Sprite("sprites/download.png",
                                        center_x=100, center_y=100)
        # self.spritemain.angle = 90
        self.spritemain.scale = 0.25


        self.right = False
        self.left = False
        self.up = False
        self.down = False

        self.play_music()

    def on_draw(self):
        arcade.start_render()
        self.spritemain.draw()

    def on_update(self, delta_time: float):
        if self.right:
            self.player_x += self.player_speed * delta_time
        if self.left:
            self.player_x -= self.player_speed * delta_time
        if self.up:
            self.player_y += self.player_speed * delta_time
        if self.down:
            self.player_y -= self.player_speed * delta_time

        self.spritemain.set_position(self.player_x, self.player_y)

    def on_key_press(self, symbol: int, modifiers: int):
        if symbol == arcade.key.RIGHT:
            self.right = True
        if symbol == arcade.key.LEFT:
            self.left = True
        if symbol == arcade.key.UP:
            self.up = True
        if symbol == arcade.key.DOWN:
            self.down = True

    def on_key_release(self, symbol: int, modifiers: int):
        if symbol == arcade.key.RIGHT:
            self.right = False
        if symbol == arcade.key.LEFT:
            self.left = False
        if symbol == arcade.key.UP:
            self.up = False
        if symbol == arcade.key.DOWN:
            self.down = False

    def adv_music(self):
        self.current_song += 1
        if self.current_song > len(self.music_list):
            self.current_song = 0

    def play_music(self):
        self.music = arcade.Sound(self.music_list[self.current_song], streaming=True)
        self.music.play(self.MUSIC_VOLUME)


GameWindow(360, 600, 'test')
arcade.run()
