from collections import deque

class Tree:
    class Node:  # Theoretically, nesting a class makes it become another attribute to the parent class.
        def __init__(self, data, parent, children=[]):
            self._data = data
            self._parent = parent  # node type
            self._children = children  # list of children nodes?

    def __init__(self, rootdata):
        self._root = self.Node(rootdata, None)
        self._size = 1

    def __len__(self):
        return self._size

    def positions(self):  # BFS and DFS are fairly similar - DFS uses Stack to find shortest path, BFS uses queue.
        stack = deque()
        stack.append(self._root)
        while len(stack) > 0:
            position = stack.pop()
            yield position
            for child in self.children(position):
                stack.appendleft(child)

    def __iter__(self):
        for position in self.positions():
            yield position._data

    def root(self):
        return self._root

    def element(self, node):
        if not isinstance(node, self.Node):
            raise TypeError('Node is not proper type.')
        return node._data

    def parent(self, node):
        if not isinstance(node, self.Node):
            raise TypeError('Node must be of proper type Node.')
        return node._parent

    def children(self, node):
        if not isinstance(node, self.Node):
            raise TypeError('Node is not proper type.')
        return node._children

    def num_children(self, node):
        if not isinstance(node, self.Node):
            raise TypeError('Node must be of proper type Node.')
        return len(node._children)

    def is_leaf(self, node):
        if not isinstance(node, self.Node):
            raise TypeError('Node is not proper type.')
        if not node._children:
            return True
        else:
            return False

    def add(self, data, parent):
        new_node = self.Node(data, parent)
        parent._children.append(parent)
        self._size += 1
        return new_node

