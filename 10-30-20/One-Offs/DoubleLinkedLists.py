#
# a doubly linked list looks like this:
#      Head <- Pointer to start        Null
#      |A| | ---> |B| | ---> |C| | ---> X
# X <- | | | <--- | | | <--- | | |
#      /\            /\
#      Data         Next and Previous
#                  Pointer that points from one node to another

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.previous = None

    def __repr__(self):
        if self.next is None:
            return f'{self.data}'
        return f'{self.data} -> {self.next}'

class DoublyLinkedList:
    def __init__(self):
        self.head = None

    def append(self, data):
        if self.head is None:
            new_node = Node(data)
            new_node.previous = None
            self.head = new_node
        else:
            new_node = Node(data)
            cur = self.head
            while cur.next:
                cur = cur.next
            cur.next = new_node
            new_node.prev = cur
            new_node.next = None

    def prepend(self, data):
        if self.head is None:
            new_node = Node(data)
            new_node.prev = None
            self.head = new_node
        else:
            new_node = Node(data)  # New node from data
            self.head.previous = new_node  # The previous node is now the value of new_node instead of None
            new_node.next = self.head  # Next node element is what was formerly head
            self.head = new_node  # the new node is now the head
            new_node.prev = None  # new new node previous value is None

    """
    def display(self):
        cur = self.head
        while cur:
            print(cur.data)
            cur = cur.next
    """

    def __repr__(self):  # "Magical Function
        return f'{self.head}'


d2list = DoublyLinkedList()
d2list.prepend(0)
d2list.append(1)
d2list.append(2)
d2list.append(3)
d2list.append(4)
d2list.prepend(5)
print(d2list)
