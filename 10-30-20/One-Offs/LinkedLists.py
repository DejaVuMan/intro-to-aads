#
# To recap:
# a singly linked list looks like this:
#      Head <- Pointer to start        Null
#      |A| | ---> |B| | ---> |C| | ---> X
#      /\            /\
#      Data         Next
#                  Pointer that points from one node to another
#
#                Array vs Linked List
# Insert/Delete | O(n) |   O(1)
# Access elem   | O(1) |   O(n)
# Contig mem?   | YES  |   NO


# Singly Linked List: Append
# Basically, wedging a node to the end of a list, displacing the null pointer

class Node:  # Every node will consistent of self and data
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):  # Def __repr__, more like Death Reaper haha
        if self.next is None:
            return f'{self.data}'
        return f'{self.data} -> {self.next}'

class LinkedList:
    def __init__(self):
        self.head = None

    def append(self, data):
        new_node = Node(data)
        if self.head is None:  # Case I : If list is empty
            self.head = new_node
            return
        last_node = self.head  # Case II: if elements exist
        while last_node.next:
            last_node = last_node.next
        last_node.next = new_node  # Append to end of list

    def prepend(self, data):
        new_node = Node(data)
        new_node.next = self.head  # We're telling the current head to become the next node
        self.head = new_node  # The new head has been prepended

    def insert(self, prev_node, data):
        new_node = Node(data)
        cur_node = self.head
        while cur_node.data is not prev_node:
            cur_node = cur_node.next
            if cur_node is None:
                print("previous node is not present in the list")
                return
        new_node.next = cur_node.next
        cur_node.next = new_node

    def remove(self, data):
        cur_node = self.head  # Generate new node to compare against
        if cur_node and cur_node.data is data:  # if the node exists and its data is equal to searched for data
            self.head = cur_node.next  # Make the head be the next element in the list
            cur_node = None  # empty the previous head
            return
        previous = None  # keep track of previous node
        while cur_node and cur_node.data is not data:  # if the node exists and its data is not equal to desired data
            previous = cur_node  # previous node becomes the current one
            cur_node = cur_node.next  # current node becomes the next one

        if cur_node is None:  # if element not found
            return
        previous.next = cur_node.next
        cur_node = None  # remove node

    def specific_remove(self, index):
        cur_node = self.head
        if index is 0:  # if we are looking at head
            self.head = cur_node.next  # simply make the head point to next data value
            cur_node = None  # remove node
            return

        previous = None
        count = 0  # Counter to iterate through and keep track - starts at 1 because we skip head
        while cur_node is not None and count is not index:
            count += 1
            previous = cur_node
            cur_node = cur_node.next

        if cur_node is None:  # if element not found
            return
        previous.next = cur_node.next
        cur_node = None  # remove node

    def length_r(self, node):
        # Base Case
        if node is None:
            return 0
        return 1 + self.length_r(node.next)

    def nodeswap(self, node_a, node_b):
        if node_a is node_b:  # if both nodes are same
            return
        cur_1 = self.head  # current node to look at is head
        prev_1 = None  # previous does not exist yet
        while cur_1 and cur_1.data is not node_a:
            prev_1 = cur_1
            cur_1 = cur_1.next
        cur_2 = self.head
        prev_2 = None
        while cur_2 and cur_2.data is not node_b:
            prev_2 = cur_2
            cur_2 = cur_2.next
        if not cur_1 or not cur_2:  # We cannot swap for an element that is not on the list!
            return
        if prev_1:  # update the pointer! Instead of going A>B>C>D, this makes A point to C.
            prev_1.next = cur_2
        else:
            self.head = cur_2
        if prev_2:
            prev_2.next = cur_1
        else:
            self.head = cur_1
        cur_1.next, cur_2.next = cur_2.next, cur_1.next  # short for swapping pointers

    def reverse(self):
        prev = None
        cur = self.head
        while cur:
            nxt = cur.next  # temporary variable to hold next node from current
            cur.next = prev  # we're saying that the next node will be the previous one
            prev = cur  # the previous node is now the current one
            cur = nxt  # the current node will now be the next one
        self.head = prev

    def __repr__(self):  # "Magical Function
        return f'{self.head}'


test = LinkedList()
test.append('A')
test.append('B')
test.append('C')
test.append('D')
print(test)
test.specific_remove(3)
print(test)
print(test.length_r(test.head))
test.nodeswap('C', 'A')
print(test)
test.reverse()
print(test)

"""
Archived method of printing list
    # The method to display all list elements can be largely similar to what we did previously.
    def printlist(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next

Archived method of reversing string recursively (larger)
    def reverse_r(self):
        def _reverse_r(cur, prev):
            if not cur:
                return prev
            nxt = cur.next
            cur.next = prev
            prev = cur
            cur = nxt
            return _reverse_r(cur, prev)
        self.head = _reverse_r(cur=self.head, prev=None)
    """
