# It's very important to note that trees have RECURSION.


from typing import Any, List, Callable

class TreeNode:
    value: Any
    children: List['TreeNode']

    def __init__(self, data, children=None):
        self.data = data
        self.children = children or []
        self.parent = None

    def isleaf(self):  # Nodes without children
        # If no parent, don't continue
        if self.children is None:
            return False
        else:
            return True

    def add(self, child):  # Adds a node given in the argument as a child
        child.parent = self  # parent node is itself
        self.children.append(child)  # append to array

    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]):
        visit(self)
        for i in self.children:
            i.for_each_deep_first(visit)

    def for_each_level_order(self, visit: Callable[['TreeNode'], None]):
        visit(self)
        queue = self.children
        for i in queue:
            visit(i)
            for j in i.children:
                queue.append(j)

    # Flat is better than nested.

    def search_tree(self, value: Any):
        if self.value == value:
            return True

    def search(self, value: Any):
        x = self
        y = value
        # checks if the value given exists in any node, using any traversion method.
        if self.for_each_deep_first(TreeNode.search_tree(x, y)) is None:
            return False
        else:
            return True

    def level(self):
        lvl = 0
        temp = self.parent
        while temp:
            lvl += 1
            temp = temp.parent

        return lvl

    def display(self):
        indentation = ' ' * self.level() * 3
        if self.parent:
            prefix = indentation + "└──"
        else:
            prefix = "*"
        print(prefix + self.data)
        if self.children:
            for child in self.children:
                child.display()

    def __repr__(self):
        return self.value


class Tree:
    root: TreeNode

    def add(self, value: Any, parent_value: Any):
        x = TreeNode
        parent_value.children.append(x)
        parent_value.children[-1].value = value

    # adds new child of value given by value, of which the parent will be the value given by parent_value

    def for_each_level_order(self, visit: Callable[['TreeNode'], None]):
        # Traverses through tree using Level Order method, beginning from the root node (aka head?)
        self.root.for_each_level_order(visit)

    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]):
        # Traverses through tree using Deep First method, beginning from the root node (aka head?)
        self.root.for_each_deep_first(visit)


test = TreeNode("Server")  # Initialize Root Node
nodeA = TreeNode("Router")  # Create nodes to add to Root Node
nodeA.add(TreeNode("Corporate VLAN"))  # parents of node A
nodeA.add(TreeNode("Firewall"))

nodeB = TreeNode("Public VLAN")  # Node we are adding to Node A as child
nodeB.add(TreeNode("Epic Gamer"))  # Child of node B which gets added to Node A

test.add(nodeA)
nodeA.add(nodeB)

test.display()
