from typing import Any, List, Callable

class TreeNode:
    value: any
    children: List['TreeNode']

    def __init__(self, data=None):
        self.data = data
        self.children = []

    def is_leaf(self, node):
        if len(node.children) is 0:
            return True
        else:
            return False

    def add(self, child: 'TreeNode'):  # TreeNode to help with understanding
        self.children.append(child)

    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]):  # visit is basically calling of the function
        visit(self)
        for node in self.children:
            node.for_each_deep_first(visit)

    def for_each_level_order(self, visit: Callable[['TreeNode'], None]):
        queue = self.children
        visit(self)
        for node in queue:
            visit(node)
            for child in node.children:
                queue.append(child)
            queue.pop(0)

    def search(self, value):
        def find(x):
            if x.data is value:
                return True

        val2 = self.for_each_level_order(find)
        if val2 is not True:
            return False
        else:
            return True

    def __repr__(self):
        return self.data

class Tree:
    root: TreeNode

    def __init__(self, node):
        self.root = node

    def Add(self, value: Any, parent: 'TreeNode'):
        parent.children.append(TreeNode(value))

    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]):
        self.root.for_each_deep_first(visit)

    def for_each_level_order(self, visit: Callable[['TreeNode'], None]):
        self.root.for_each_level_order(visit)


trial = TreeNode("Test")
trial.add(TreeNode("Epic"))
print(trial.is_leaf(trial))
print(trial.search("Epic"))
