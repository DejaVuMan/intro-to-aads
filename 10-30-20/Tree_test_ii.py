class NNNException(Exception):
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return repr(self.value)


class Node:
    def __init__(self, key, children=None):
        self.key = key
        self.children = children or []

    def __str__(self):
        return str(self.key)


class Tree:
    def __init__(self):
        self.root = None
        self.size = 0

    def find(self, node, key):
        if node is None or node.key == key:
            return node
        for child in node.children:
            return_node = self.find(child, key)
            if return_node:
                return return_node
        return None

    def add(self, new_key, parent_key=None):
        new_node = Node(new_key)
        if parent_key is None:
            self.root = new_node
            self.size = 1
        else:
            parent_node = self.find(self.root, parent_key)
            if not parent_node:
                raise NNNException('Parent node not found...')
            parent_node.children.append(new_node)
            self.size += 1

    def disp(self, node, aux):
        if node is None:
            return ""
        aux = aux + str(node) + '('
        for i in range(len(node.children)):
            child = node.children[i]
            end = ',' if i < len(node.children) -1 else ''
            aux = self.disp(child, aux)
        return aux + ')'

    def __str__(self):
        return self.disp(self.root, "")


if __name__ == "__main__":
    tree = Tree()
    tree.add('a')

    tree.add('b', 'a')
    tree.add('e', 'b')

    tree.add('d', 'b')
    tree.add('h', 'd')

    tree.add('i', 'e')
    tree.add('j', 'e')

    print(tree)
