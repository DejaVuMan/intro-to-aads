# What is a tree?
#
# A non-linear hierarchical data structure containing nodes connected by edges.
#
#               A
#              / \
#             B   C
#           / | \
#          D  E  F
#
# Very similar to a Linked List. Each node points to another, some nodes can point to many.
# Node B has 3 children: D, E, and F. It is Parented by A, which is the root node.
# Nodes can be called "Leaf Nodes."
#
# Moving through a Tree:
# We generally start from the Root Node and move down the tree.
#
# Level Order
# Scan every tree by level (lvl 1 = Root, lvl 2 = connecting to root, etc...)
# So, the order in the example tree would be: A -> B -> C -> D -> E -> F
#
# Also
#
# Deep First
# We continue down each individual branch
# When we reach branch, we go to first child we find
# So, the order in the example tree would be: A -> B -> D -> E -> F -> C
