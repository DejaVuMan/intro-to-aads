from typing import Any


class BinaryNode:
    value: Any
    left_child: 'BinaryNode'
    right_child: 'BinaryNode'

    def __init__(self, data):
        self.data = data
        self.left_child = None
        self.right_child = None
        # Set value of added node

    def min(self):
        if self.left_child is not None:
            return self.left_child.min()
        return self

    # return node with smallest value from selected node

    def _contains(self, data: Any):
        if self.data is data:
            return True
        elif self.data > data:
            if hasattr(self, 'left_child'):
                return self.left_child._contains(data)
            else:
                return False
        else:
            if hasattr(self, 'right_child'):
                return self.right_child._contains(data)
            else:
                return False

    def show(self, level):
        if self.right_child!=None:
            self.right_child.show(level+1)
        print(' ' * 4 * level + '->', self.data)
        if self.left_child!=None:
            self.left_child.show(level+1)



class BinarySearchTree:
    root: BinaryNode

    def __init__(self, node: 'BinaryNode'):
        self.root = node

    def insert(self, data: Any):
        self.root = self._insert(self.root, data)
        # -> none
        # add new node using private _insert method, then returns new data structure
        # and sets it as the root.

    def _insert(self, node: BinaryNode, data: Any):
        if data < node.data:
            if node.left_child is None:
                node.left_child = BinaryNode(data)
            else:
                self._insert(node.left_child, data)
        else:
            if node.right_child is None:
                node.right_child = BinaryNode(data)
            else:
                self._insert(node.right_child, data)
        return node
        # add new node

    def insertlist(self, userList):
        for val in userList:
            self.insert(val)
        # Adds many nodes in a serial fashion to the tree by calling the insert method multiple times

    def contains(self, data: Any):
        return self.root._contains(data)
        # search for specific node

    def remove(self, data: Any):
        self.root = self._remove(self.root, data)
        # -> none
        # remove a specific node using private _remove method, and then returns the new data structure and sets it as
        # the root.

    def _remove(self, node: BinaryNode, data: Any):
        if node is not None:
            if data == node.value:
                if (node.left_child is None) & (node.right_child is None):
                    return None
                elif node.left_child is None:
                    return node.right_child
                elif node.right_child is None:
                    return node.left_child
                node.data = node.right_child.min().data
                node.right_child = self._remove(node.right_child, node.data)
            elif data < node.data:
                node.left_child = self._remove(node.left_child, data)
            else:
                node.right_child = self._remove(node.right_child, data)
        return node
        # remove node

    def show(self):
       self.root.show(0)

tree = BinarySearchTree(BinaryNode(5))
tree.insert(3)
tree.insert(8)
tree.insert(7)

tree.show()

print(tree.root.min().data)
