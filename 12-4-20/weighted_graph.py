from enum import Enum
from typing import Any
from typing import Optional
from typing import Dict, List
from graphviz import Digraph as abracadabra

puzzle = __import__("OneWayList_WW")


class Queue:
    def __init__(self):
        self.queue = puzzle.LinkedList()

    def __len__(self):
        return len(self.queue)

    def __repr__(self):
        holder = self.queue.head.next
        # the .next part here is necessary to not display the
        # "dummy" starter element of none.
        disp = str(holder.data)
        holder = holder.next
        while holder is not None:
            disp += ' -> ' + str(holder.data)
            holder = holder.next
        return disp

    def peekaboo(self):
        return self.queue.head.value

    def enq(self, data):
        self.queue.append(data)

    def deq(self):
        return self.queue.pop()


class EdgeType(Enum):
    directed = 1
    undirected = 2


class Vertex:
    data: Any
    index: int

    def __init__(self, data, idx):
        self.data = data
        self.index = idx

    def __repr__(self):
        return f'{self.data} of index {self.index}'


class Edge:
    source: Vertex
    destination: Vertex
    weight: Optional[float]

    def __init__(self, source, dest, imp):
        self.source = source
        self.destination = dest
        self.weight = imp
        # Weight is not 100% needed but can be used if desired

    def __repr__(self):
        return f'Target: {self.destination}'


class Graph:
    adjacency: Dict[Vertex, List[Edge]]

    def __init__(self):
        self.adjacency = dict()

    def create_vertex(self, data: Any):
        self.adjacency[Vertex(data, len(self.adjacency))] = list()

    def add_diredge(self, source: Vertex, destination: Vertex,
                    weight: Optional[float] is None):
        driver.adjacency[source].append(Edge(source, destination, weight))
        # by setting driver equal to Graph() outside of this code, we can basically run this code properly for the
        # instance of that name
        # Will only add one directional support to next node

    def add_anyedge(self, source: Vertex, destination: Vertex,
                    weight: Optional[float] is None):
        driver.adjacency[source].append(Edge(source, destination, weight))
        driver.adjacency[destination].append(Edge(source, destination, weight))
        # Will add bi directional support to next node

    def add(self, edge: EdgeType, source: Vertex, destination: Vertex,
            weight: Optional[float] = None):
        if edge.name is "directed":
            self.add_diredge(source, destination, weight)
        else:
            self.add_anyedge(source, destination, weight)

    def depth_first_trav(self, goto):
        visited = []
        klist = []
        for ct in self.adjacency.keys():
            klist.append(ct)
        new = klist[0]
        self._dfs(new, visited, goto)

    def _dfs(self, v: Vertex, viewed: List[Vertex], visit):
        visit(v)
        viewed.append(v)
        for next_node in self.adjacency[v]:
            if next_node.destination in viewed:
                return True
            else:
                self._dfs(next_node.destination, viewed, visit)

    def breadth_first_trav(self, goto):
        visited = []
        klist = []
        for ct in self.adjacency.keys():
            klist.append(ct)
        line = Queue()
        line.enq(klist[0])

        while len(line) != 0:
            new_n = line.deq()
            visited.append(new_n)
            goto(new_n)
            for next_node in self.adjacency[new_n]:
                if next_node.destination in visited:
                    return True
                else:
                    line.enq(next_node.destination)

    def show(self, name: Optional = "graph"):
        noder = abracadabra(comment='driver')
        self._show(self.adjacency[list(self.adjacency.keys())[0]][0].source, abracadabra, list())
        abracadabra.render(f'intro-to-aads/11-27-20/{name}', view=True, format="png", quiet_view=False)

    def _show(self, v: Vertex, abracadabra, visited: List):
        abracadabra.node(str(v.index), str(v.data))
        visited.append(v)
        for neighbor in self.adjacency[v]:
            desc = ""
            if neighbor.weight != None:
                desc += f"{neighbor.weight}"
            abracadabra.edge(str(neighbor.source.index), str(neighbor.destination.index), label=desc)
            if not (neighbor.destination in visited):
                self._show(neighbor.destination, abracadabra, visited)

    def __repr__(self):
        string = ""
        for data in self.adjacency:
            string += f'-{data} -> {self.adjacency[data]}\n'
        return string


class GraphPath:
    cost: Dict[Vertex, int]
    parents: Dict[Vertex, List[Vertex]]
    visited: List[Vertex]
    frost: List[Vertex]

    def __init__(self, graph: Graph, v: Vertex, e: Vertex, name: Optional ="driver"):
        self.graph = driver
        self.costs = dict()
        self.parents = dict()
        self.visited = list()
        self.path = list()
        for child in graph.adjacency.keys():
            self.costs[child] = 9999999
            self.parents[child] = None
        for neighbor in graph.adjacency[v]:
            self.costs[neighbor.destination] = neighbor.weight
            self.parents[neighbor.destination] = neighbor.source
        self._short(graph, v, e)
        nodes = abracadabra(comment="")
        visited = []
        for x in graph.adjacency.keys():
            self._show(x, nodes, visited, graph)

    def _short(self, graph: Graph, v: Vertex, e: Vertex):
        w = [y for x in graph.adjacency.values() for y in x]
        if w[0].weight is None:
            self._BFS(graph, v, e)
        self._dijkstry(graph, v, e)
        print(self.path)

    def _show(self, v: Vertex, nodes, visited: List, graph: Graph):
        if v in visited:
            True
        else:
            nodes.node(str(v.index), str(v.data))
            visited.append(v)
            for neighbor in graph.adjacency[v]:
                desc = ""
                if neighbor.weight is not None:
                    desc += f"{neighbor.weight}"
                path = False
                for x in range(1, len(self.path)):
                    if (self.path[x - 1] == neighbor.source) & self.path[x] == neighbor.destination:
                        path = True
                    if path:
                        nodes.edge(str(neighbor.source.index),
                                   str(neighbor.destination.index), label=desc, color="blue")
                    else:
                        nodes.edge(str(neighbor.source.index),
                                   str(neighbor.destination.index), label=desc)
                    if not (neighbor.destination in visited):
                        self._show(neighbor.destination, nodes, visited, graph)

    def _dijkstry(self, graph: Graph, v: Vertex, e: Vertex):
        self.visited.append(v)
        k = min(self.costs, key=self.costs.get)
        while k is not None:
            self.visited.append(k)
            cost = self.costs[k]
            for edge in graph.adjacency[k]:
                if self.costs[edge.destination] > (cost + edge.weight):
                    self.costs[edge.destination] = (cost + edge.weight)
                    self.costs[edge.destination] = edge.source
                copied = self.costs.copy()
                # v = min(copied, key=copied.get)
                while min(self.costs.copy(), key=self.costs.copy().get) in self.visited:
                    copied.pop(min(copied, key=copied.get))
                    if len(copied.keys()) == 0:
                        # v = None
                        break
                    # v = min(copied, key=copied.get)
        while e is not None:
            self.path.append(e)
            e = self.parents[e]
        self.path.reverse()

    def _BFS(self, graph: Graph, v: Vertex, e: Vertex):
        line = Queue()
        line.enq([v])
        while len(line) is not 0:
            dq = line.deq()
            dqvert = dq[-1]
            for neighbor in graph.adjacency[dqvert]:
                if neighbor.destination not in self.visited:
                    dn = dq.copy()
                    dn.append(neighbor.destination)
                    self.visited.append(neighbor.destination)
                    line.enq(dn)
                    if neighbor.destination == e:
                        self.path = dn

def name_of_global_obj(xx):
    for objname, oid in globals().items():
        if oid is xx:
            return objname


driver = Graph()
driver.create_vertex(10)
driver.create_vertex(20)
driver.create_vertex(69)
driver.create_vertex("final")

tester = driver.adjacency.keys()
keylist = []
for x in tester:
    keylist.append(x)
driver.add(EdgeType(1), keylist[0], keylist[1], 1)  # 10 targets 20, weight of 1
driver.add(EdgeType(1), keylist[0], keylist[2], 3)  # 10 targets 69, weight of 3
driver.add(EdgeType(1), keylist[2], keylist[3], 10)  # 69 targets "final", weight of 10

print(driver.adjacency)
driver.depth_first_trav(print)
print()
driver.breadth_first_trav(print)
print()

print(driver)

path = GraphPath(driver, keylist[0], keylist[3])

# driver.show(name=name_of_global_obj(driver))
# Error Traceback to line 130 in node
# name = self._quote(name) str obj has no attribute _quote?

"""queue = Queue()

assert len(queue) == 0

queue.enq('test')
queue.enq('test2')

print(str(queue))
print(len(queue))"""
