from enum import Enum
from typing import Any, Optional, Dict, List, Callable
import networkx as nx
import matplotlib.pyplot as plt

puzzle = __import__("OneWayList_WW")


class Queue:
    def __init__(self):
        self.queue = puzzle.LinkedList()

    def __len__(self):
        return len(self.queue)

    def __repr__(self):
        holder = self.queue.head.next
        # the .next part here is necessary to not display the
        # "dummy" starter element of none.
        disp = str(holder.data)
        holder = holder.next
        while holder is not None:
            disp += ' -> ' + str(holder.data)
            holder = holder.next
        return disp

    def peekaboo(self):
        return self.queue.head.value

    def enq(self, data):
        self.queue.append(data)

    def deq(self):
        return self.queue.pop()


class EdgeType(Enum):
    directed = 1
    undirected = 2


class Vertex:
    data: Any
    index: int  # position of value in graph

    def __init__(self, data):
        self.data = data  # data value

    def __str__(self):
        return self.data  # for when we want to represent the data


class Edge:
    destination: Vertex
    source: Vertex
    weight: Optional[float]  # Weight of a path to help decide shortest route

    def __init__(self, source, destination, weight):
        self.source = source  # Originating from node
        self.destination = destination  # Targeted node
        self.weight = weight

    def __str__(self):
        return str(self.source) + " " + str(self.destination)


class Graph:
    adjacent: Dict[Vertex, List[Edge]]

    def __init__(self):
        self.adjacent = {}  # Begin with empty
        self.currentIndex = 0  # Starting index
        self.dgraph = nx.DiGraph()
        self.graph = nx.Graph()

    def __str__(self):  # to display nodes if we print
        string = ""
        for key in list(self.adjacent.keys()):
            string = string + str(key) + " - "
            for y in self.adjacent[key]:
                string += "[" + str(y.source) + " -> " + str(y.destination) + "]"  # Node - [Node Dest]
            string += '\n'
        return string

    def create_vertex(self, data: Any) -> Vertex:
        vertex = Vertex(data=data)
        self.adjacent.update({vertex: []})
        vertex.index = self.currentIndex
        self.currentIndex += 1
        return vertex

    def add_directed_edge(self, source: Vertex, destination: Vertex, weight: Optional[int] = None):
        edge = Edge(source=source, destination=destination, weight=weight)
        self.adjacent[source].append(edge)
        e = (source, destination)
        self.dgraph.add_nodes_from([source, destination])
        self.dgraph.add_edge(*e, weight=weight, r=weight)

    def add_undirected_edge(self, source: Vertex, destination: Vertex, weight: Optional[int] = None):
        self.add_directed_edge(source=source, destination=destination, weight=weight)
        edge = Edge(source=destination, destination=source, weight=weight)
        self.adjacent[destination].append(edge)
        e = (source, destination)
        self.graph.add_nodes_from([source, destination])
        self.graph.add_edge(*e)

    def add(self, edge: EdgeType, source: Vertex, destination: Vertex, weight: Optional[int] = None):
        if edge == EdgeType.directed:
            self.add_directed_edge(source=source, destination=destination, weight=weight)
        elif edge == EdgeType.undirected:
            self.add_undirected_edge(source=source, destination=destination)

    def traverse_depth_first(self, visit: Callable[[Any], None]):
        first = list(self.adjacent.keys())[0]
        values = list(self.adjacent.values())
        visited = []

        def dfs(v: Vertex, visited: List[Vertex], visit: Callable[[Any], None]):
            visit(v)
            visited.append(v)
            for x in values[v.index]:
                if x.destination not in visited:
                    dfs(x.destination, visited, visit)

        dfs(first, visited, visit)

    def dshow(self):
        pos = nx.spring_layout(self.dgraph)
        plt.figure()
        nx.draw(self.dgraph, pos, edge_color='black',
                width=1, with_labels=True, node_size=500, node_color='pink', alpha=0.9,
                labels={node: node for node in self.dgraph.nodes()})
        edge_labels = nx.get_edge_attributes(self.dgraph, 'r')
        nx.draw_networkx_edge_labels(self.dgraph, pos, edge_labels=edge_labels)
        plt.show()  # display

    def gshow(self):
        pos = nx.spring_layout(self.graph)
        plt.figure()
        nx.draw(self.graph, pos, edge_color='black',
                width=1, with_labels=True, node_size=500, node_color='pink', alpha=0.75,
                labels={node: node for node in self.dgraph.nodes()})
        plt.show()


class GraphPath:
    visited: List[Vertex]

    def __init__(self, graph: Graph, v: Vertex, x: Vertex):
        self.graph = graph
        self.v = v
        self.x = x
        self.neighbours = self.graph.adjacent[self.v]
        self.visited = list()
        self.bpath = list()

    def _min_cost(self, cost_table: Dict):
        lc = 9999  # Set high initial cost
        cheapest = None
        visited = []
        for x in cost_table:
            if cost_table[x] < lc and x not in visited:
                lc = cost_table[x]
                cheapest = x
                visited.append(x)
        return cheapest

    def dijkstra(self):
        cost_table = {}
        for x in self.neighbours:
            cost_table[x.destination] = x.weight
        cost_table[self.x] = 99999
        parent_table = {}
        for x in self.neighbours:
            parent_table[x.destination] = self.v
        parent_table[self.x] = None
        vertex = self._min_cost(cost_table)
        visited = []
        path = {}
        while vertex is not None and vertex not in visited:
            c = cost_table.get(vertex)
            path[vertex] = c
            for x in self.graph.adjacent[vertex]:
                nc = c + x.weight
                imp = cost_table.get(x.destination)
                if imp is None:
                    imp = 0
                if imp > nc:  # can raise TypeError- '>' not supported b/w NoneType and int???
                    cost_table[x.destination] = nc
                    parent_table[x.destination] = vertex
            visited.append(vertex)
            try:
                vertex = self.graph.adjacent[vertex][0].destination
            except IndexError:
                vertex = None
        for p in path:
            print(str(p) + ":" + str(path[p]))

    def BFS(self, graph: Graph, v: Vertex, x: Vertex):
        line = Queue()
        line.enq([v])
        while len(line) is not 0:
            dq = line.deq()
            dqvert = dq[-1]
            for neighbor in graph.adjacent[dqvert]:
                if neighbor.destination not in self.visited:
                    dn = dq.copy()
                    dn.append(neighbor.destination)
                    self.visited.append(neighbor.destination)
                    line.enq(dn)
                    if neighbor.destination == x:
                        self.bpath = dn
        for p in self.bpath:
            print(str(p))
