puzzle = __import__("weighted_graph_v2")  # import weighted_graph_v2, and OneWayList_WW at the same time

driver = puzzle.Graph()
A = driver.create_vertex('A')  # create the node/vertex
B = driver.create_vertex('B')
C = driver.create_vertex('C')
D = driver.create_vertex('D')
# E = driver.create_vertex('E')

driver.add(puzzle.EdgeType.directed, A, B, 25)  # puzzle.EdgeType.x, Start, Dest, Weight(opt)
driver.add(puzzle.EdgeType.directed, A, C, 8)
driver.add(puzzle.EdgeType.directed, C, B, 4)
driver.add(puzzle.EdgeType.directed, C, D, 7)
driver.add(puzzle.EdgeType.directed, B, D, 1)
# driver.add(puzzle.EdgeType.directed, B, E, 10)
# driver.add(puzzle.EdgeType.directed, D, E, 30)
# Having two paths seems to work a bit better but not entiirely?
# After debugging, the error seems to occurr because the Dijkstra algorithm does not have a

print(driver)

path = puzzle.GraphPath(driver, C, D)
# In some Circumstances, algorithm will throw an error ( A to C/B for example)
# It seems as if the final problem is due to the next added node not having any value???
# Results in Saying that Final node sum of weights is None...
# Trying to go from A to E results in error @ nc = c + x.weight, unsupported for NoneType and Int???

# Line 185 in dijkstra -> if cost_table[x.destination] > nc:
# KeyError: <weighted_graph_v2.Vertex object at 0x00000269FC775BC8>

path.dijkstra()  # use dijkstra algorithm

driver.dshow()  # Use gshow for undirected
