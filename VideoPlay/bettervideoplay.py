import cv2
# ffpyplayer for playing audio
from ffpyplayer.player import MediaPlayer

video_path = "C://Users/szusz/Desktop/UWM Sophomore Year/Algorithms and Data Structures/lasaga.mp4"

window_name = "window"

def playvideo(video_path):
    video = cv2.VideoCapture(video_path)
    player = MediaPlayer(video_path)

    cv2.namedWindow(window_name, cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    while True:
        grabbed, frame = video.read()
        audio_frame, val = player.get_frame()
        if not grabbed:
            print("End of video")
            break
        if cv2.waitKey(100) & 0xFF == ord("q"):
            break
        cv2.imshow("Video", frame)
        if val != 'eof' and audio_frame is not None:
            # audio
            img, t = audio_frame
    video.release()
    cv2.destroyAllWindows()


playvideo(video_path)
