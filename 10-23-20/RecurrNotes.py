# Python Lab 3 - Recursion (Rekurencja)

# Python accepts Function Recursion, meaning that a defined function can actually call itself.

# A common mathematical/programming concept, means you can loop through data to reach a result.

# Use with CAUTION. Easy to accidentally make a program which loops forever or uses too many available resources.

# When written effectively, can be very efficient.

# Example function below:
#   We have a function which calls itself ("recurse"). K is an int variable which decreases by 1 every time we recurse.
#   It'll end when it is not greater than 0.

def recursion(k):
    if k > 0:
        result = k+recursion(k-1)
        # the result is equivalent to the value k is given plus the value of recursion(value - 1)
        print(result)
    else:
        result = 0
    return result


print("Recursion results: ")
recursion(6)

# another example below:
# We set a value and the recursing function keeps subtracting by 1 until it reaches 0 and stops.


def rec(i):
    if i < 0:
        return

    print(f'i: {i}')

    rec(i - 1)


rec(10)


# A recursive function MUST have a Recursive case and a base case.
# The recursive case decomposes the original problem into simpler instances of the same problem
# The base case is the smallest instance of the same problem.

# Lets look at factorials.

# n! = n * (n-1) * (n-2) * (n-3) * .... * 1
# n! = n * (n-1)!
# this can lead to a very natural recursive solution.
# 5! = 5 * 4 * 3 * 2 * 1
# 5! = 5 * 4!
"""
def fac_rec(n):
    return n * fac_rec(n-1)
"""
# If left in just this state, we would be waiting until the end of time...
# We need a BASE CASE!


def fac_rec(n):
    if n == 1:
        return 1
    return n * fac_rec(n-1)


print(fac_rec(5))  # 120
