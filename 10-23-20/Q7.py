# Create the function n_sums(n: int), which returns a list/array [int].
# The function will return all n-digit numbers with the same sums on even and odd indices.
# example:
# 3 digit value: 198, 220, 891, 990
#  1+8 = 9, 2+0 = 2, 8+1 = 1, 9+0 = 9


def n_sums(n: int):
    if n is 2:
        n = 10
    elif n is 3:
        n = 100
    elif n is 4:
        n = 1000
    elif n is 9 or 99:
        return result

    splitter = [int(let) for let in str(n)]
    holder_odd = 0
    holder_even = 0
    for index, j in enumerate(splitter):
        if (index+1) % 2 == 0:
            holder_even = holder_even + j
        else:
            holder_odd = holder_odd + j

    if holder_odd == holder_even:
        result.append(n)

    return n_sums(n+1)


result = []
print(n_sums(2))
