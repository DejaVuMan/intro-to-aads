# Create the function power(number: int, n: int) which returns number^n.

def power(number: int, n: int):
    if n == 0:
        return 1
    elif n == 1:
        return number
    else:
        return number * power(number, n-1)

print(power(5, 3))

# Basically
# 5 * power(5 ,2)
#     5 * power(5, 1)
#     5 * 5
# Basically -> 5 * (5 * 5)
# Which is 125.
