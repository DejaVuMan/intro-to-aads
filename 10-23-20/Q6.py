# Make a function prime(n: int) which returns a boolean indicating if a number is a prime number.

# Isn't python really bad for recursion? It has no tail call optimization

def prime(n: int, i: int = 2):
    if n < 2:   # Numbers below 2 aren't considered prime.
        return False
    if i + 1 == n:  # if the values add up to n by 1, then it is probably not divisible by any, so we can return true.
        return True
    if 2 >= n > 0:
        return True
    if n % i == 0:  # Checks if the number is divisible by any value from 2 to n.
        return False
    return prime(n, i+1)  # keeps iterating until i is 1 below n.


print(prime(13))  # True
print(prime(7))  # True
print(prime(4))  # False
