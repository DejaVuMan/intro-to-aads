# Create a function numbers(n: int) which displays all numbers from n to 0.

def numbers(n: int):
    if n < 0:
        return
    print(n, end=" ")
    numbers(n-1)


numbers(12)
