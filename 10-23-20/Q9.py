# Recursion which removes duplicates

def duplicate_r(string):
    if not string:
        return ""
    if len(string) is 1:
        return string
    if string[0] is string[1]:
        return duplicate_r(string[1:])
    return string[0] + duplicate_r(string[1:])


print(duplicate_r('XXYZZZ'))
