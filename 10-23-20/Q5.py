# Create a function factorial(n: int) which returns the factorial value of a number.

def fac_rec(n):
    if n == 1:
        return 1
    return n * fac_rec(n-1)


print(fac_rec(4))
# 4 * fac_rec(4-1)
#     3 * fac_rec(3-1)
#         2 * fac_rec(2-1)
#         2 * 1
