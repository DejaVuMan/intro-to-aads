# Create function reverse(txt: str) which returns a reversed string of numbers from a text document.

def reverse(s):

    if s == "":  # if the current value we're looking at is empty, we simply return that empty field.
        return s
    else:
        return reverse(s[1:]) + s[0]
        # Fun fact -- [::-1] is called a "martian smiley!"
        # By doing s[1:], we're saying to ignore the first element in the list, and then add the first one to new list.


f = open("tester.txt", "r")
contents = f.read()
print(reverse(contents))

# It looks like so:
# Sample test: 12345
# s[2345] + s[1]
#           s[345] + s[2]
#                    s[45] + s[3]
#                            s[5] + s[4]
#                                    s[] + s[5]
