# Print a certain element of the fibbonacci sequence as fib(n: int) which counts the nth number sequence.
# I might be partially stupid - I really thought we were supposed to print EVERY element up to that one out.

def fib(n: int):
    if n is 1:
        return 0
    elif n is 2:
        return 1
    else:
        return fib(n-1) + fib(n-2)


print(fib(4))  # 2
print(fib(5))  # 3
print(fib(5))  # 5

# This method has some memory and time issues. Eventually, there are noticeable delays ~fib(28).
# Call trees are relevant?
# to compute fib(5), you need 15 calls
# to compute fib(10), 177.
# to compute fib(20), 21,891.
# Memorization is allegedly very relevant here for remembering often used values.
