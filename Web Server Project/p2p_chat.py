import socket
import threading
import time
from random import randint
import sys
import winsound

class Server:
    connections = []
    peers = []

    def __init__(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # to access socket library
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('0.0.0.0', 10000))
        sock.listen(1)
        winsound.Beep(2500, 500)  # Frequency, Duration (in ms)
        print("Server is running...")

        while True:  # This loop actually handles the connection
            c, a = sock.accept()
            cThread = threading.Thread(target=self.handler, args=(c, a))  # allow function to use multiple threads
            cThread.daemon = True  # Allow program to exit with threads still running
            cThread.start()
            self.connections.append(c)
            self.peers.append(a[0])
            print(str(a[0]) + ':' + str(a[1]), "connected")
            self.sendPeers()

    def handler(self, c, a):
        while True:
            data = c.recv(1024)  # Max data received: 1024 bytes.
            for connection in self.connections:
                connection.send(data)
            if not data:
                print(str(a[0]) + ':' + str(a[1]), "disconnected")
                self.connections.remove(c)
                self.peers.remove(a[0])
                c.close()
                self.sendPeers()
                break  # loop won't run until data received

    def sendPeers(self):
        p = ""
        for peer in self.peers:
            p = p + peer + ","

        for connection in self.connections:
            connection.send(b'\x11' + bytes(p, 'utf-8'))  # \x11 means we are sending it at the beginning of the string

class Client:
    def sendMsg(self, sock):
        while True:
            sock.send(bytes(input(""), 'utf-8'))

    def __init__(self, address):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.connect((address, 10000))

        iThread = threading.Thread(target=self.sendMsg, args=(sock,))  # Passed a tubble
        iThread.daemon = True
        iThread.start()

        while True:
            data = sock.recv(1024)  # 1024 bytes of data
            if not data:
                break
            if data[0:1] == b'\x11':
                self.updatePeers(data[1:])
            else:
                count = 0
                count = len(p2p.peers) - 1
                winsound.Beep(400, 250)
                print(str(p2p.peers[count]) + " said: " + str(data, 'utf-8'))
                print("\nStart Typing: ", end="")

    def updatePeers(self, peerData):
        p2p.peers = str(peerData, "utf-8").split(",")[:-1]

class p2p:  # We must make this class so that the list can be accessed from any class
    peers = ['192.168.8.103']  # default peer IP address - use your own local IP for testing
    # TODO: Real Automated Peer Discovery and Update

while True:
    try:
        print("Trying to connect...")
        time.sleep(randint(1, 5))
        for peer in p2p.peers:
            try:
                client = Client(peer)
            except KeyboardInterrupt:
                sys.exit(0)
            except:
                pass
            if randint(1, 5) == 1:
                try:
                    server = Server()  # Our client tries to become the server
                except KeyboardInterrupt:
                    sys.exit(0)
                except:
                    print("Couldn't start the server...")

    except KeyboardInterrupt:
        sys.exit(0)  # Press Ctrl C on your keyboard to exit
