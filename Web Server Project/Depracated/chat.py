# Hi! This isn't directly related to Algorithms, but is a cool little side project for networking and
# a chat communication app.
#
import socket
import threading  # to allow multiple connections at once

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # to access socket library
#                       Use IPv4    |    TCP Connection
#        we use a TCP instead of a UDP connection to ensure data transmission.

sock.bind(('0.0.0.0', 10000))
# 0.0.0.0 makes the server available for any IP address configured on it
# 10000 is our port, it can be configured to anything.

sock.listen(1)  # Maximum number of queued connections

connections = []

def handler(c, a):
    while True:
        global connections
        data = c.recv(1024)  # Max data received: 1024 bytes.
        for connection in connections:
                connection.send(bytes(data))
        if not data:
            connections.remove(c)
            c.close()
            break  # loop won't run until data received


while True:  # This loop actually handles the connection
    c, a = sock.accept()
    cThread = threading.Thread(target=handler, args=(c, a))  # allow function to use multiple threads
    cThread.daemon = True  # Allow program to exit with threads still running
    cThread.start()
    connections.append(c)
    print(connections)
