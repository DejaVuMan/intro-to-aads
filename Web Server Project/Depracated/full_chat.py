import socket
import threading

class Server:
    connections = []
    peers = []

    def __init__(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # to access socket library
        sock.bind(('0.0.0.0', 10000))
        sock.listen(1)

        while True:  # This loop actually handles the connection
            c, a = sock.accept()
            cThread = threading.Thread(target=self.handler, args=(c, a))  # allow function to use multiple threads
            cThread.daemon = True  # Allow program to exit with threads still running
            cThread.start()
            self.connections.append(c)
            self.peers.append(a[0])
            print(str(a[0]) + ':' + str(a[1]), "connected")
            self.sendPeers()

    def handler(self, c, a):
        while True:
            data = c.recv(1024)  # Max data received: 1024 bytes.
            for connection in self.connections:
                connection.send(data)
            if not data:
                print(str(a[0]) + ':' + str(a[1]), "disconnected")
                self.connections.remove(c)
                self.peers.remove(a[0])
                c.close()
                self.sendPeers()
                break  # loop won't run until data received

    def sendPeers(self):
        p = ""
        for peer in self.peers:
            p = p + peer + ","

        for connection in self.connections:
            connection.send(b'\x11' + bytes(p, "utf-8"))  # \x11 means we are sending it at the beginning of the string

class Client:
    def sendMsg(self, sock):
        while True:
            sock.send(bytes(input(""), 'utf-8'))

    def __init__(self, address):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((address, 10000))

        iThread = threading.Thread(target=self.sendMsg, args=(sock,))  # Passed a tubble
        iThread.daemon = True
        iThread.start()

        while True:
            data = sock.recv(1024)  # 1024 bytes of data
            if not data:
                break
            if data[0:1] == b'\x11':
                print("Received new Peers...")
            print(str(data, 'utf-8'))


x = input("what mode are you running this application in? enter an IP address for client: ")
if(len(x)) > 1:
    client = Client(x)
    pass
else:
    server = Server()
