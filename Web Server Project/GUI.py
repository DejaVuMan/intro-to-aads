import tkinter as tk
import sys
import threading
puzzle = __import__('p2p_chat')

def starterC():
    for puzzle.peer in puzzle.p2p.peers:
        try:
            client = puzzle.Client(puzzle.peer)
        except KeyboardInterrupt:
            sys.exit(0)

def starterS():
    server = puzzle.Server()

def rTs(self):
    content = e.get().encode()
    puzzle.Client.sendMsg(self, sock=content)


root = tk.Tk()  # Kind of like the body attribute in HTML
root.title("Instant Messaging App Alpha")

canvas = tk.Canvas(root, height=700, width=700, bg="#263D42")
canvas.pack()

frame = tk.Frame(root, bg="white")
frame.place(relwidth=0.8, relheight=0.8, relx=0.1, rely=0.1)

startS = tk.Button(root, text="Start Server...", padx=10, pady=5,
                   command=threading.Thread(target=starterS).start)
startS.pack(side=tk.LEFT)

startC = tk.Button(root, text="Start Client...", padx=10, pady=5, fg="white", bg="black",
                   command=threading.Thread(target=starterC).start)
startC.pack(side=tk.RIGHT)

text = tk.Text(frame)
text.pack()

text.insert(tk.END, "test")

indic = tk.Label(frame, text="Please enter a sentence: ")
indic.pack()

userin = ''
e = tk.Entry(root, textvariable=userin, fg='lime', bg='black')
e.pack()
e.bind('<Return>', rTs)

root.mainloop()
