import socket
import threading
import tkinter as tk

window = tk.Tk()
window.title("Server")

# Top frame with bottons start and stop
topFrame = tk.Frame(window)

btnStart = tk.Button(topFrame, text="Connect", command=lambda: start_server())
btnStart.pack(side=tk.LEFT)

btnStop = tk.Button(topFrame, text="Stop", command=lambda: stop_server())
btnStop.pack(side=tk.RIGHT)

topFrame.pack(side=tk.TOP, pady=(5, 0))

# Middle frame with labels for host and port info
middleFrame = tk.Frame(window)

hostlab = tk.Label(middleFrame, text="Host: X.X.X.X")
hostlab.pack(side=tk.LEFT)

portlab = tk.Label(middleFrame, text="Port: XXXX")
portlab.pack(side=tk.LEFT)

middleFrame.pack(side=tk.TOP, pady=(5, 0))

# Client frame shows client info, messages, etc
clientFrame = tk.Frame(window)

linelab = tk.Label(clientFrame, text="=-=-=-=-=-=-=Client List=-=-=-=-=-=-=").pack()

scrollBar = tk.Scrollbar(clientFrame)
scrollBar.pack(side=tk.RIGHT, fill=tk.Y)

tkDisplay = tk.Text(clientFrame, height=15, width=40)
tkDisplay.pack()

# Demo usernames
tkDisplay.insert(tk.END, "User1\n")
tkDisplay.insert(tk.END, "User2\n")
tkDisplay.insert(tk.END, "User3\n")
tkDisplay.insert(tk.END, "User4\n")

scrollBar.config(command=tkDisplay.yview)
tkDisplay.config(yscrollcommand=scrollBar.set, background="#F4F6F7",
                 highlightbackground="grey", state="disabled")
clientFrame.pack(side=tk.BOTTOM, pady=(5, 0))

# Server Side Logic #

server = None
HOST_ADDR = "192.168.8.103"
HOST_PORT = 10000
client_name = " "
clients = []
clients_names = []

# Start Server


def start_server():
    global server, HOST_ADDR, HOST_PORT  # code would be ok without this
    btnStart.config(state=tk.DISABLED)
    btnStop.config(state=tk.NORMAL)

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(("0.0.0.0", 10000))
    server.listen(5)  # listens to client connection

    hostlab["text"] = "Host: " + HOST_ADDR
    portlab["text"] = "Port: " + str(HOST_PORT)


athread = threading.Thread(target=start_server)
athread.daemon = True
athread.start()

def accept_clients(the_server, y):
    bthread = threading.Thread(target=accept_clients)
    bthread.daemon = True
    bthread.start()
    while True:
        client, addr = the_server.accept()
        clients.append(client)


def send_receive_client_message(client_connection, client_ip_addr):
    global server, client_name, clients, clients_addr
    client_msg = " "

    # send welcome message to client
    client_name = client_connection.recv(4096)
    client_connection.send("Welcome " + client_name + ". Use 'exit' to quit")

    clients_names.append(client_name)

    update_client_names_display(clients_names)  # update client names display

    while True:
        data = client_connection.recv(4096)  # 4096 bytes
        if not data:
            break
        if data == "exit":
            break

        client_msg = data

        idx = get_client_index(clients, client_connection)
        sending_client_name = clients_names[idx]

        for c in clients:
            if c != client_connection:
                c.send(sending_client_name + "->" + client_msg)

    # find client index
    idx = get_client_index(clients, client_connection)
    del clients_names[idx]
    del clients[idx]
    client_connection.send("BYE!")
    client_connection.close()

    update_client_names_display(clients_names)  # update client names display

def get_client_index(client_list, curr_client):
    idx = 0
    for conn in client_list:
        if conn == curr_client:
            break
        idx = idx + 1

    return idx

def update_client_names_display(name_list):
    tkDisplay.config(state=tk.NORMAL)
    tkDisplay.delete('1.0', tk.END)

    for c in name_list:
        tkDisplay.insert(tk.END, c+"\n")
    tkDisplay.config(state=tk.DISABLED)

def stop_server():
    btnStart.config(state=tk.NORMAL)
    btnStop.config(state=tk.DISABLED)


window.mainloop()
