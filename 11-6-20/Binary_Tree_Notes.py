#  Binary Tree: Lab 5 Notes

# A Binary Tree is a specific example of a N-ary tree (General tree), in which each node has no more than
# TWO children (it's "level" is 3).

# Each binary tree node contains a reference to, at most, two children on the LEFT and RIGHT.

#                                   EXAMPLE BINARY TREE
#                                            10
#                                           /  \
#                                          9    2
#                                         /\    /\
#                                        1  3  4  6
#
#   NAVIGATING a BINARY TREE
# each method defines a separate order of visiting nodes in the tree starting from the designated node,
# which is usually the root.
#
#   In Order Traversal
# We start traversing the tree from the left most leaf, then we go to the parent and right-most child.
# We then go to the parent two levels higher and repeat the above pattern.
#
# In the example tree, traversal is: 1 -> 9 -> 3 -> 10 -> 4 -> 2 -> 6
#
#   Post Order Traversal
# We start traversing the tree from the left most leaf, then we go to the right neighbor and their common parent.
# Then we head to the leaf of the adjacent node on the same level and repeat the above scheme.
#
# In the example tree, traversal is: 1 -> 3 -> 9 -> 4 -> 6 -> 2 -> 10
#
#   Pre Order Traversal
# We start traversing from the designated node (i.e, root) and visit its left child, and then the left child
# one node down. After reaching the leaf, we visit the neighboring right node, and repeat for the right child of root.
# In the example tree, traversal is: 10 -> 9 -> 1 -> 3 -> 2 -> 4 -> 6
