class Node(object):
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

class BinaryTree(object):
    def __init__(self, root):
        self.root = Node(root)

    def disp_tree(self, travtype):
        if travtype == 1:
            return self.preorder_disp(self.root, "")
        if travtype == 2:
            return self.inorder_disp(self.root, "")
        else:
            print("Traversal type is not an option!")
            return False

    def preorder_disp(self, start, trav):
        if start:
            trav += (str(start.value) + "-")
            trav = self.preorder_disp(start.left, trav)
            trav = self.preorder_disp(start.right, trav)
        return trav

    def inorder_disp(self, start, trav):
        if start:
            trav = self.inorder_disp(start.left, trav)
            trav += str(start.value) + "-"
            trav = self.inorder_disp(start.right, trav)
        return trav


binary = BinaryTree(1)
binary.root.left = Node(2)
binary.root.right = Node(3)
binary.root.left.left = Node(4)
print(binary.disp_tree(1))
