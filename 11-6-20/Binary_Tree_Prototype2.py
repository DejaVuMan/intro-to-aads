from typing import Any, Callable


class BinaryNode:
    value: Any
    left_child: 'BinaryNode'
    right_child: 'BinaryNode'

    def __init__(self, data):
        self.data = data
        self.left_child = None
        self.right_child = None
        # removed previous left_child and right_child declarations as it would make every node have them.

    def is_leaf(self):
        if hasattr(self, 'left_child') or hasattr(self, 'right_child'):
            return True
        return False

    def add_left_child(self, data: Any):
        self.left_child = BinaryNode(data)

    def add_right_child(self, data: Any):
        self.right_child = BinaryNode(data)

    def traverse_in_order(self, visit: Callable[[Any], None]):
        if self.left_child is not None:
            self.left_child.traverse_in_order(visit)
        visit(self)
        if self.right_child is not None:
            self.right_child.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]):
        if self.left_child is not None:
            self.left_child.traverse_post_order(visit)
        if self.right_child is not None:
            self.right_child.traverse_post_order(visit)
        visit(self)

    def traverse_pre_order(self, visit: Callable[[Any], None]):
        visit(self.data)
        if hasattr(self, 'left_child'):
            self.left_child.traverse_pre_order(visit)
        if hasattr(self, 'right_child'):
            self.right_child.traverse_pre_order(visit)

    def search(self, value):
        def find(nodevalue):  # Updated to differentiate b/w value searched for and node value
            if value == nodevalue:
                self.found = True
                # This modification makes it technically less efficient as every node now needs to
                # to store an extra value in memory
                # we should default this back to "false" as well, or else we start with = true every time.

        self.traverse_pre_order(find)
        if hasattr(self, 'found'):
            del self.found
            # delete the search variable so it doesnt take up memory.
            # allows us to correctly search again as well.
            return True
        else:
            return False

    def show(self, lv):
        if hasattr(self, 'right_child'):
            self.right_child.show(lv+1)
        if lv == 0:
            print(self.data)
        elif lv == 1 and hasattr(self, 'left_child'):
            print(' ' + u'\u2514' + u'\u2500', self.data)
        elif lv == 1:
            print(' ' + u'\u250c' + u'\u2500', self.data)
        elif hasattr(self, 'left_child'):
            print(' ' * 2 * lv + u'\u2514' + u'\u2500', self.data)
        else:
            print(' ' * 2 * lv + u'\u251c' + u'\u2500', self.data)
        # Ending node ends in ├ because unable to verify where node has children.

        if hasattr(self, 'left_child'):
            self.left_child.show(lv+1)

    def illus(self, node):
        print(node, end=' ')

    def __repr__(self):
        return f'{self.data}'

    def __str__(self):
        return str(self.data)


class BinaryTree:
    root: BinaryNode

    def __init__(self, root):
        self.root = BinaryNode(root)

    def illus(self, node):
        print(node, end=' ')

    def traverse_in_order(self, visit: Callable[[Any], None]):
        self.root.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]):
        self.root.traverse_post_order(visit)

    def traverse_pre_order(self, visit: Callable[['Any'], None]):
        self.root.traverse_pre_order(visit)

    def show(self):
        self.root.show(0)

primer = BinaryTree(10)
primer.root.add_left_child(9)
primer.root.add_right_child(2)

primer.root.traverse_in_order(primer.root.illus)
print('\n')
primer.root.traverse_post_order(primer.root.illus)
