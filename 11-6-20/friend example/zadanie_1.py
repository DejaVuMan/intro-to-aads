from typing import Any, Callable

class BinaryNode:
    value: Any
    left_child: 'BinaryNode'
    right_child: 'BinaryNode'

    def __init__(self, value: Any):
        self.value = value
        self.left_child = None
        self.right_child = None
        
    def is_leaf(self):
        if(self.left_child == None and self.right_child == None):
            return True
        else:
            return False

    def add_left_child(self, value: Any):
        self.left_child = BinaryNode(value)

    def add_right_child(self, value: Any):
        self.right_child = BinaryNode(value)
        
    def traverse_in_order(self, visit: Callable[[Any], None]):
        if(self.left_child != None):
            self.left_child.traverse_in_order(visit)
        visit(self)
        if(self.right_child != None):
            self.right_child.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]):
        if(self.left_child != None):
            self.left_child.traverse_post_order(visit)
        if(self.right_child != None):
            self.right_child.traverse_post_order(visit)
        visit(self)

    def traverse_pre_order(self, visit: Callable[[Any], None]):
        visit(self)
        if(self.left_child != None):
            self.left_child.traverse_pre_order(visit)
        if(self.right_child != None):
            self.right_child.traverse_pre_order(visit)

    def show_consol_version(self, order=''):
        for i in range(0, len(order), 1):
            if(order[i] == 'L'):
                if(i == len(order)-1):
                    print('┣━━━━', end='')
                else:
                    print('┃    ', end='')
            if(order[i] == 'R'):
                if(i == len(order)-1):
                    print('┗━━━━', end='')
                else:
                    print('     ', end='')
        print(self)
        if(self.left_child != None):
            self.left_child.show_consol_version(order+'L')
        if(self.right_child != None):
            self.right_child.show_consol_version(order+'R')

    def generate_html(self, file, indent=' '*4*5):
        file.writelines(indent+'<div class="value">'+str(self.value)+'</div>\n')
        if(self.left_child != None):
            file.writelines(indent+'<div class="left_child">\n')
            self.left_child.generate_html(file, indent+'    ')
        else:
            if(self.right_child != None):
                file.writelines(indent+'<div class="left_child_none">\n')
                file.writelines(indent+'    <div class="value_none"></div>\n')
                file.writelines(indent+'</div>\n')
        if(self.right_child != None):
            file.writelines(indent+'<div class="right_child">\n')
            self.right_child.generate_html(file, indent+'    ')
        else:
            if(self.left_child != None):
                file.writelines(indent+'<div class="right_child_none">\n')
                file.writelines(indent+'    <div class="value_none"></div>\n')
                file.writelines(indent+'</div>\n')
        indent = indent[:-4]
        file.writelines(indent+'</div>\n')

    def show_html_version(self, output_file_name, object_name):
        result_html = open(output_file_name+'.html', 'w')
        with open('pattern.html', 'r') as pattern_html:
            for line in pattern_html:
                result_html.writelines(line)
                if(line == '            <div id="description">\n'):
                    indent = ' ' * 4 * 4
                    result_html.writelines(indent+object_name+'\n')
                if(line == '                </svg>\n'):
                    result_html.writelines('                <div id="root">\n')
                    self.generate_html(result_html)
        result_html.close()

    def __repr__(self):
        return str(self.value)

class BinaryTree:
    root: BinaryNode

    def __init__(self, value):
        self.root = BinaryNode(value)

    def traverse_in_order(self, visit: Callable[[Any], None]):
        self.root.traverse_in_order(visit)
    
    def traverse_post_order(self, visit: Callable[[Any], None]):
        self.root.traverse_post_order(visit)

    def traverse_pre_order(self, visit: Callable[[Any], None]):
        self.root.traverse_pre_order(visit)

    def show(self):
        print('tekstowa reprezentacja drzewa:\n')
        print('value')
        print('┣━━━left')
        print('┗━━━right\n')
        self.root.show_consol_version()
        output_file_name = 'show'
        object_name = str(self)
        self.root.show_html_version(output_file_name, object_name)
        print('\n!!! Został wygenerowany plik "'+str(output_file_name)+'.html" z graficzną repreznatacją drzewa!!!')

tree = BinaryTree(10)
tree.root.add_left_child(9)
tree.root.left_child.add_left_child(1)
tree.root.left_child.add_right_child(3)
tree.root.add_right_child(2)
tree.root.right_child.add_left_child(4)
tree.root.right_child.left_child.add_right_child(7)
tree.root.right_child.add_right_child(6)
tree.root.right_child.right_child.add_left_child(111)

assert tree.root.value == 10
assert tree.root.right_child.value == 2
assert tree.root.right_child.is_leaf() is False
assert tree.root.left_child.left_child.value == 1
assert tree.root.left_child.left_child.is_leaf() is True

tree.show()