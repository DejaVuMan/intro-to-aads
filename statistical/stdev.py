import statistics

# creating a simple data - set
sample = []

print("Enter a value to append to the sample list, or type '999' to stop.")
while True:
# Note: Python 2.x users should use raw_input, the equivalent of 3.x's input
    data = float(input("Enter your value: "))
    if data != 999:
        sample.append(data)
        print("prepare next value")
    else:
        print("array set")
        break


# Prints standard deviation
# xbar is set to default value of 1
print("Standard Deviation of sample is % s "
      % (statistics.stdev(sample)))
