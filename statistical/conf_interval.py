import numpy as np
import scipy.stats


def mean_confidence_interval(values, confidence):
    a = 1.0 * np.array(values)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    print(m)
    print(m-h)
    print(m+h)


sample = []

print("Enter a value to append to the sample list, or type '999' to stop.")
while True:
    # Note: Python 2.x users should use raw_input, the equivalent of 3.x's input
    data = float(input("Enter your value: "))
    if data != 999:
        sample.append(data)
        print("prepare next value")
    else:
        print("array set")
        break

x = float(input("Confidence Interval?: "))

mean_confidence_interval(sample, x)
